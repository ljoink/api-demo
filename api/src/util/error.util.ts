import { Request, Response, NextFunction } from "express";
const handler = (err: any, req: Request, res: Response, next: NextFunction) => {
  // TODO handle next
  res.status(500);
  res.render("error", { error: err });
};

export default { handler };
