import express, { Request, Response } from "express";
import userController from "./controller/user.controller";
import packageJson from "../package.json";
import consultationController from "./controller/consultation.controller";
import jwtUtil from "./util/jwt.util";

const router = express.Router();

router.get("/version", (_req: Request, res: Response) => {
  res.send(`v${packageJson.version}`);
});

router.post(
  "/user/create",
  userController.createValidator,
  userController.create
);

router.post("/user/login", userController.loginValidator, userController.login);

router.post(
  "/consultation/create",
  jwtUtil.check,
  consultationController.createValidator,
  consultationController.create
);
router.get(
  "/consultation/list",
  jwtUtil.check,
  consultationController.getValidator,
  consultationController.get
);

export default router;
