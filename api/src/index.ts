import express from "express";
import cors from "cors";
import { config } from "dotenv";
config();

import router from "./router";
import envUtil from "./util/env.util";
import error from "./util/error.util";

const app = express();
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cors());

app.use("/", router);
app.use(error.handler);

const port = envUtil.get("PORT");
app.listen(port, () =>
  console.log(`api listening at http://localhost:${port}`)
);
