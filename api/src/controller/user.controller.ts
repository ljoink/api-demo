import { NextFunction, Request, Response } from "express";
import { body, validationResult } from "express-validator";
import bcrypt from "bcrypt";
import userModel from "../model/user.model";
import jwt from "jsonwebtoken";
import envUtil from "../util/env.util";
import jwtUtil from "../util/jwt.util";

const createValidator = [
  body("email")
    .notEmpty()
    .withMessage("'email' is required")
    .isEmail()
    .isLength({ max: 50 })
    .withMessage("Invalid 'email'"),

  body("password")
    .notEmpty()
    .withMessage("'password' is required")
    .isLength({ min: 8, max: 30 })
    .withMessage("Invalid 'password' (8 - 30 characters)"),

  body("phone")
    .notEmpty()
    .withMessage("'phone' is required")
    .isLength({ min: 8, max: 20 })
    .withMessage("Invalid 'phone' (8 - 20 characters)"),

  body("address")
    .notEmpty()
    .withMessage("'address' is required")
    .isLength({ max: 300 })
    .withMessage("Invalid 'address'"),
];

const create = async (req: Request, res: Response, next: NextFunction) => {
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    return res.status(400).json({ errors: errors.array() });
  }

  const { email, password, phone, address } = req.body;

  userModel.findOneByEmail(email, async (err, user) => {
    if (err) {
      return next(err);
    }
    if (user) {
      return res.status(400).send("User exists");
    }

    const hashPassword = await bcrypt.hash(password, 10);
    userModel.create(email, hashPassword, phone, address, (err) => {
      if (err) {
        return next(err);
      } else {
        return res.send("User created");
      }
    });
  });
};

const loginValidator = [
  body("email").notEmpty().withMessage("'email' is required"),
  body("password").notEmpty().withMessage("'password' is required"),
];

const login = async (req: Request, res: Response, next: NextFunction) => {
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    return res.status(400).json({ errors: errors.array() });
  }

  const { email, password } = req.body;

  userModel.findOneByEmail(email, async (err, user) => {
    if (err) {
      return next(err);
    }
    if (!user) {
      return res.status(400).send("Invalid email or password");
    }

    const isMatch = await bcrypt.compare(password, user.password!);
    if (!isMatch) {
      return res.status(400).send("Invalid email or password");
    }

    delete user.password;
    const token = jwtUtil.newToken(user);

    res.send(token);
  });
};

export default {
  createValidator,
  create,
  loginValidator,
  login,
};
