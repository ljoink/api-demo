import dbService from "../service/db.service";
import moment from "moment";

const create = (
  createBy: number,
  clinic: string,
  doctorName: string,
  patientName: string,
  diagnosis: string,
  medication: string,
  consultationFee: number,
  date: string,
  followUp: string,
  callback: (err: any) => void
): void => {
  const sql = `
    INSERT INTO consultation
      (
        createBy,
        clinic,
        doctor_name,
        patient_name,
        diagnosis,
        medication,
        consultation_fee,
        date, 
        follow_up
      )
      VALUES(?, ?, ?, ?, ?, ?, ?, ?, ?)
  `;
  const values = [
    createBy,
    clinic,
    doctorName,
    patientName,
    diagnosis,
    medication,
    consultationFee,
    date,
    followUp === "true" ? 1 : 0,
  ];

  dbService.getDb().query({ sql, values }, (err) => {
    callback(err);
  });
};

type TConsultation = {
  createBy: number;
  clinic: string;
  doctorName: string;
  patientName: string;
  diagnosis: string;
  medication: string;
  consultationFee: number;
  date: Date;
  followUp: number;
};

const get = (
  {
    from,
    to,
    limit = "25",
    offset = "0",
  }: { from?: string; to?: string; limit?: string; offset?: string },
  callback: (err: any, consultations: TConsultation[] | null) => void
) => {
  let where = "";
  if (from && to) {
    const format = "YYYY-MM-DD HH:mm:ss";
    const startFrom = moment(from).startOf("day").format(format);
    const endTo = moment(to).endOf("day").format(format);
    where = `WHERE date BETWEEN '${startFrom}' AND '${endTo}'`;
  }

  const sql = `
    SELECT *
      FROM consultation
      ${where}
      LIMIT ?
      OFFSET ?
  `;
  const values = [parseInt(limit), parseInt(offset)];

  dbService.getDb().query({ sql, values }, (err, rows) => {
    callback(err, JSON.parse(JSON.stringify(rows)));
  });
};

export default {
  create,
  get,
};
