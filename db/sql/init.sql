CREATE DATABASE db;

USE db;

CREATE TABLE user (
  id INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
  email VARCHAR(50),
  password VARCHAR(300),
  phone_number VARCHAR(20),
  address VARCHAR(300)
) ENGINE=InnoDB;

CREATE TABLE consultation (
  id INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
  createBy INT NOT NULL,
  clinic VARCHAR(50),
  doctor_name VARCHAR(50),
  patient_name VARCHAR(50),
  diagnosis VARCHAR(50),
  medication VARCHAR(50),
  consultation_fee int NOT NULL,
  date datetime NOT NULL,
  follow_up tinyint(1) NOT NULL
) ENGINE=InnoDB;