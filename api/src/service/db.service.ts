import mysql from "mysql";
import envUtil from "../util/env.util";

class DbService {
  private db: mysql.Connection | null = null;
  private dbName = envUtil.get("DB_NAME");

  getDb = (): mysql.Connection => {
    if (!this.db) {
      try {
        this.db = mysql.createConnection({
          host: envUtil.get("DB_HOST"),
          user: envUtil.get("DB_USER"),
          password: envUtil.get("DB_PASSWORD"),
          port: parseInt(envUtil.get("DB_PORT")),
        });

        this.db.query(`USE ${this.dbName}`);
      } catch (err) {
        throw err;
      }
    }

    return this.db as mysql.Connection;
  };
}

export default new DbService();
