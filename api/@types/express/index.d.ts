import { TCurrentUser } from "../../src/model/user.model";

declare global {
  declare namespace Express {
    interface Request {
      currentUser?: TCurrentUser;
    }
  }
}
