- Build and run api and db container

```
docker-compose up
```

or

Bash Script to start the project

```
./start.sh
```

- Import ./api_demo.postman_collection.json to Postman for api test data

- This project using Adminer for accessing MySql

```
http://localhost:8080
```

- This project using swagger as API Doc, please copy & paste ./swagger.ymal to:

```
https://editor.swagger.io/
```
