import { NextFunction, Request, Response } from "express";
import { body, query, validationResult } from "express-validator";
import consultationModel from "../model/consultation.model";

const createValidator = [
  body("clinic")
    .notEmpty()
    .isLength({ max: 50 })
    .withMessage("'clinic' is required"),

  body("doctorName")
    .notEmpty()
    .isLength({ max: 50 })
    .withMessage("'doctorName' is required"),

  body("patientName")
    .notEmpty()
    .isLength({ max: 50 })
    .withMessage("'patientName' is required"),

  body("diagnosis")
    .notEmpty()
    .isLength({ max: 50 })
    .withMessage("'diagnosis' is required"),

  body("medication")
    .notEmpty()
    .isLength({ max: 50 })
    .withMessage("'medication' is required"),

  body("consultationFee")
    .notEmpty()
    .isInt()
    .withMessage("'consultationFee' is required"),

  body("date")
    .notEmpty()
    .withMessage("'date' is required")
    .matches(
      /^(?:199[0-9]|20[0-9][0-9])-(?:0[1-9]|1[0-2])-(?:[0-2][0-9]|3[0-1]) (?:[0-1][0-9]|2[0-3]):[0-5][0-9]:[0-5][0-9]$/
    )
    .withMessage("'date' must be in YYYY-MM-DD HH:mm:ss"),

  body("followUp")
    .notEmpty()
    .withMessage("'followUp' is required")
    .isBoolean()
    .withMessage("Invalid 'followUp'"),
];

const create = (req: Request, res: Response, next: NextFunction) => {
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    return res.status(400).json({ errors: errors.array() });
  }

  const {
    clinic,
    doctorName,
    patientName,
    diagnosis,
    medication,
    consultationFee,
    date,
    followUp,
  } = req.body;

  consultationModel.create(
    req.currentUser!.id,
    clinic,
    doctorName,
    patientName,
    diagnosis,
    medication,
    consultationFee,
    date,
    followUp,
    (err) => {
      console.log(
        "Tindy err\n",
        JSON.parse(JSON.stringify(err || "undefined"))
      );
      if (err) {
        return next(err);
      } else {
        return res.send("Consultation created");
      }
    }
  );
};

const getValidator = [
  query("from")
    .optional()
    .matches(
      /^(?:199[0-9]|20[0-9][0-9])-(?:0[1-9]|1[0-2])-(?:[0-2][0-9]|3[0-1])$/
    )
    .withMessage("'from' must be in YYYY-MM-DD"),
  query("to")
    .optional()
    .matches(
      /^(?:199[0-9]|20[0-9][0-9])-(?:0[1-9]|1[0-2])-(?:[0-2][0-9]|3[0-1])$/
    )
    .optional()
    .withMessage("'to' must be in YYYY-MM-DD"),
  query("limit").optional().isInt().withMessage("Limit invalid"),
  query("offset").optional().isInt().withMessage("Offset invalid"),
];

const get = (req: Request, res: Response, next: NextFunction) => {
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    return res.status(400).json({ errors: errors.array() });
  }

  consultationModel.get(req.query, (err, rows) => {
    if (err) {
      return next(err);
    } else {
      return res.send(rows);
    }
  });
};

export default {
  createValidator,
  create,
  getValidator,
  get,
};
