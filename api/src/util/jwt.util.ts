import { NextFunction, Request, Response } from "express";
import envUtil from "./env.util";
import jwt from "jsonwebtoken";
import { TUser } from "../model/user.model";

const newToken = (user: TUser): string => {
  return jwt.sign({ user }, envUtil.get("JWT_SECRET"));
};

const check = (req: Request, res: Response, next: NextFunction) => {
  const authHeader = req.headers["authorization"] || "";

  const tokenArr = authHeader.split(" ");
  if (tokenArr[0] === "") {
    return res.status(400).send("Token missing");
  }
  if (tokenArr[0] !== "Bearer" || tokenArr.length != 2) {
    return res.status(400).send("Token format error");
  }

  const token = tokenArr[1];
  jwt.verify(token, envUtil.get("JWT_SECRET"), (err, payload) => {
    if (err) {
      next(err);
    } else {
      req.currentUser = payload?.user;
      next();
    }
  });
};

export default { newToken, check };
