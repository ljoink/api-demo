import dbService from "../service/db.service";

export type TUser = {
  email: string;
  password?: string;
  phone: string;
  address: string;
};

export type TCurrentUser = Omit<TUser, "password"> & { id: number };

const findOneByEmail = (
  email: string,
  callback: (err: any, user: TUser | null) => void
): void => {
  const sql = `
    SELECT *
      FROM user
      WHERE email=?
  `;
  const values = [email];

  dbService.getDb().query({ sql, values }, (err, rows) => {
    const user = rows ? JSON.parse(JSON.stringify(rows))[0] : null;
    callback(err, user);
  });
};

const create = (
  email: string,
  hashPassword: string,
  phone: string,
  address: string,
  callback: (err: any) => void
): void => {
  const sql = `
    INSERT INTO user
      (email, password, phone_number, address)
      VALUES(?, ?, ?, ?)
  `;
  const values = [email, hashPassword, phone, address];

  dbService.getDb().query({ sql, values }, (err) => {
    callback(err);
  });
};

export default {
  findOneByEmail,
  create,
};
